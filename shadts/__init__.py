import os
import gitlab
import redis

from flask import session, Flask, url_for


app = Flask(__name__)
app.config.from_envvar('SHADTS_SETTINGS', silent=True)


# Fix resource caching
@app.context_processor
def override_url_for():
    return {"url_for": dated_url_for}


def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path, endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)


def make_gitlab_api():
    return gitlab.Gitlab(app.config["GITLAB_URL"], app.config["GITLAB_ADMIN_TOKEN"])


def is_gitlab_user_exists(username):
    gitlab_api = make_gitlab_api()
    user = gitlab_api.users.list(username=username)
    return len(user) == 1


def save_user_info(username, firstname, lastname, groupname):
    client = redis.StrictRedis(app.config["REDIS"])
    client.hset("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "firstname", firstname.lower())
    client.hset("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "lastname", lastname.lower())
    client.hset("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "groupname", groupname)


def has_user_info(username):
    client = redis.StrictRedis(app.config["REDIS"])
    return client.hget("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "firstname") is not None


def format_project_name(username, firstname, lastname, groupname):
    projectname = '{}-{}-u-{}'.format(firstname, lastname, username)
    if need_show_group_names():
        return (groupname) + '-' + projectname
    return projectname


def get_all_user_info(username):
    client = redis.StrictRedis(app.config["REDIS"])
    firstname = client.hget("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "firstname")
    lastname = client.hget("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "lastname")
    groupname = client.hget("user_info:{}:{}".format(app.config["COURSE_NAME"], username), "groupname")
    if firstname is None or lastname is None or groupname is None:
        raise ValueError('Unregistred user: try register first before login')

    firstname = firstname.decode('utf-8')
    lastname = lastname.decode('utf-8')
    groupname = groupname.decode('utf-8')
    return firstname, lastname, groupname


def get_project_name_for_user(username):
    firstname, lastname, groupname = get_all_user_info(username)
    return format_project_name(username, firstname, lastname, groupname)


def url_for_student_repo(username):
    return app.config["GITLAB_URL"] + "/" + \
           app.config["GITLAB_GROUP"] + "/" + \
           get_project_name_for_user(username)


def url_for_student_submits(username):
    return app.config["GITLAB_URL"] + "/" + \
           app.config["GITLAB_GROUP"] + "/" + \
           get_project_name_for_user(username) + "/-/jobs"


@app.template_global()
def navbar():
    student_repo_url = url_for_student_repo(session["gitlab"]["username"])
    student_submits_url = url_for_student_submits(session["gitlab"]["username"])

    links = [
        ("/", "tasks", "Tasks"),
        (student_repo_url, "repo", "My Repo"),
        (student_submits_url, "submits", "submits"),
    ]

    if "CRASHME_KEY" in app.config:
        links += [("/submit", "submit", "submit flag")]

    links += [("/logout", "logout", "Logout")]

    return links


@app.template_global()
def need_show_group_names():
    return "ACCEPTABLE_GROUPS" in app.config


from . import login
from . import signup
from . import api
from . import course
from . import arbitrage

# def decode_flag(flag):
#     msg, sig = flag[5:-1].rsplit(':', 1)
#     challenge, date = msg.split(":", 1)

#     print(sig, hmac.new(CRASHME_KEY.encode("utf8"), msg=msg.encode("utf8")).hexdigest())
#     if sig != hmac.new(CRASHME_KEY.encode("utf8"), msg=msg.encode("utf8")).hexdigest():
#         raise ValueError("invalid signature")

#     return challenge, date

# @app.route("/submit", methods=["GET", "POST"])
# @requires_auth
# def submit():
#     if request.method == "GET":
#         return render_template("submit.html")

#     flag = request.form["flag"]

#     try:
#         challenge, date = decode_flag(flag)
#     except ValueError as ex:
#         return render_template("submit.html", error_message=ex.args[0])

#     user_id = int(session["gitlab"]["id"])
#     user = gitlab_api.users.get(user_id)
#     login = user.username
#     repo_url = GITLAB_URL + "/" + GITLAB_GROUP + "/" + login

#     sheet = gdoc.get_sheet()
#     task_score = get_task_score(challenge, gdoc.is_deadline_extended_for_login(sheet, login))
#     gdoc.put_score_in_gdoc(sheet, challenge, login, task_score, user.name, repo_url)

#     pipe = redis.pipeline()
#     pipe.hset("result:{}:{}".format(login, challenge), "ok", "1")
#     pipe.hset("result:{}:{}".format(login, challenge), "flag", flag)
#     pipe.execute()

#     return redirect(url_for("main_page"))

