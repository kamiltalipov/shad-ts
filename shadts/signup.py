import logging
import gitlab
import re

from flask import request, session, render_template, redirect, url_for

from . import app, make_gitlab_api, is_gitlab_user_exists, save_user_info, has_user_info, get_project_name_for_user


logger = logging.getLogger(__name__)


def register_new_user(username, firstname, lastname, email, password):
    gitlab_api = make_gitlab_api()

    logger.info("Creating user: {}".format({
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "email": email,
    }))

    new_user = gitlab_api.users.create({
        "email": email,
        "username": username,
        "name": firstname + " " + lastname,
        "external": False,
        "password": password,
        "confirm": False,
    })
    logger.info("Gitlab user created {}".format(new_user))


def _find_existing_project(gitlab, course_group, project_name):
    projects = list(filter(lambda project: project.name == project_name,
        course_group.projects.list(search=project_name)))

    if len(projects) > 0:
        return gitlab.projects.get(projects[0].id)

    return None

def check_master_branch(project):
    branches = project.branches.list()
    for branch in branches:
        if branch.name == "master":
            return branch
    return None

def has_runner(project, runner_id):
    runners = project.runners.list()
    for runner in runners:
        if runner.id == runner_id:
            return True
    return False

def has_user(project, user_id):
    for user in project.members.list():
        if user.id == user_id:
            return True
    return False

def maybe_create_project_for_user(username, user_id):
    gitlab_api = make_gitlab_api()

    project_name = get_project_name_for_user(username)
    course_group = gitlab_api.groups.get(app.config["GITLAB_GROUP"])

    # Find or create repository

    project = _find_existing_project(gitlab_api, course_group, project_name)

    if not project:
        print("existing project not found, create new")
        project = gitlab_api.projects.create({
            "name": project_name,
            "namespace_id": course_group.id,
            "builds_enabled": True,
            "with_merge_requests_enabled": True,
            "shared_runners_enabled": False,
        })
    else:
        print("project alrady exists")
        logger.info("Project already exists: {}".format(project_name))

    # Prepare master

    print("prepare master")

    has_master = check_master_branch(project)

    if not has_master:
        print("create readme.md")
        project.files.create({
            'file_path': 'readme.md',
            'content': '# Solutions repository',
            'branch': 'master',
            'author_email': 'tpcc_course@example.com',
            'author_name': 'tpcc',
            'commit_message': 'initial commit',
        })

    print("protect master")
    project.branches.get('master').protect()

    # Enable specific runnner

    print("setup runner")

    ci_runner_id = int(app.config["CI_RUNNER_ID"]) if "CI_RUNNER_ID" in app.config else None

    if ci_runner_id:
        if not has_runner(project, ci_runner_id):
            print("runner not found")
            project.runners.create({'runner_id': ci_runner_id})
            logger.info("Set runner {} for project {}".format(ci_runner_id, project))

    logger.info("Git project created {}".format(project))

    # Add user to project


    if not has_user(project, user_id):
        print("register project member")
        member = project.members.create({
            "user_id": user_id,
            "project_id": project.id,
            "access_level": gitlab.DEVELOPER_ACCESS,
        })

    print ("completed")

    logger.info("Access to project granted to {}".format(user_id))


def is_acceptable_group_name(group_name):
    if "ACCEPTABLE_GROUPS" in app.config:
        acceptable_groups = app.config["ACCEPTABLE_GROUPS"].split(',')
        return group_name in acceptable_groups
    return True


NAME_REGEXP=re.compile('^[a-zA-Z]+$')
def is_acceptable_name(name):
    return NAME_REGEXP.match(name) is not None


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "GET":
        return render_template("signup.html")

    if request.form["secret"] != app.config["REGISTRATION_SECRET"]:
        return render_template("signup.html", error_message="Invalid secret code")

    group_name = request.form["group"] if "ACCEPTABLE_GROUPS" in app.config else "0"

    if not is_acceptable_name(request.form["firstname"]):
        return render_template("signup.html", error_message="Invalid first name: only latin letters accepted")
    if not is_acceptable_name(request.form["lastname"]):
        return render_template("signup.html", error_message="Invalid last name: only latin letters accepted")
    if not is_acceptable_group_name(group_name):
        return render_template("signup.html", error_message="Invalid group number")
    if not is_gitlab_user_exists(request.form["username"]):
        return render_template("signup.html", error_message="Invalid username: nonexistent Gitalab user")

    if has_user_info(request.form["username"]):
        return render_template("signup.html", error_message="User with this username is already registered")

    save_user_info(
        request.form["username"],
        request.form["firstname"],
        request.form["lastname"],
        group_name)

    return redirect(url_for("login"))
