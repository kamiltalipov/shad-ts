import logging
import pytz
import yaml
import datetime
import requests
import flask
import redis

from flask import session, render_template

from . import app
from .login import requires_auth


logger = logging.getLogger(__name__)
MOSCOW_TIMEZONE = pytz.timezone('Europe/Moscow')


def parse_time(time):
    date = datetime.datetime.strptime(time, "%d-%m-%Y %H:%M")
    return MOSCOW_TIMEZONE.localize(date)


class ScoreCalculator:
    def __init__(self, deadline, 
                 fall_period=datetime.timedelta(days=7),
                 end_penalty=0.5):
        self.deadline = deadline
        self.fall_period = fall_period
        self.end_penalty = end_penalty

    def score(self, full_score, submit_time):
        # In time

        if submit_time <= self.deadline:
            return full_score

        # Too late

        if submit_time >= self.deadline + self.fall_period:
            return full_score * self.end_penalty

        # Score decreases linearly

        late = submit_time - self.deadline

        fall_period_seconds = int(self.fall_period.total_seconds())
        late_seconds = int(late.total_seconds())

        score_loss_limit = int(full_score * (1 - self.end_penalty))

        score_penalty = (score_loss_limit * late_seconds) // fall_period_seconds

        return full_score - score_penalty


class Task:
    def __init__(self, group, config):
        self.group = group
        self.name = config["task"]
        self.score = config["score"]
        self.tags = config.get("tags", [])
        self.max_attempts = config.get("max_attempts")

    @property
    def homework(self):
        return self.name.split('/')[0]

    @property
    def short_name(self):
        return self.name.split('/')[1]

    def has_attempts_left(self, attempt):
        if self.max_attempts is not None:
            return attempt < self.max_attempts
        return True

    def is_overdue(self, extra_time=datetime.timedelta()):
        return datetime.datetime.now(MOSCOW_TIMEZONE) > self.group.deadline + extra_time

    def get_current_score(self):
        now = datetime.datetime.now(MOSCOW_TIMEZONE)
        return self.group.score_calculator.score(full_score=self.score, submit_time=now)



class Group:
    def __init__(self, config):
        self.name = config["group"]
        self.start = parse_time(config["start"])
        self.deadline = parse_time(config["deadline"])
        self.pretty_deadline = config["deadline"]
        self.tasks = [Task(self, c) for c in config.get("tasks", [])]
        self.score_calculator = ScoreCalculator(self.deadline)

    @property
    def is_open(self):
        return datetime.datetime.now(MOSCOW_TIMEZONE) > self.start


class Deadlines:
    def __init__(self, config):
        self.groups = [Group(c) for c in config]

    def find_task(self, name):
        for g in self.groups:
            for task in g.tasks:
                if task.name == name:
                    return task
        raise KeyError("Task {} not found".format(name))

    @property
    def open_groups(self):
        if session["gitlab"].get("is_admin", False):
            return self.groups
        return [g for g in self.groups if g.is_open]

    @property
    def task_names(self):
        names = []
        for g in self.groups:
            for task in g.tasks:
                names.append(task.name)
        return names

    @classmethod
    def fetch(cls):
        deadlines_url = "{}/{}/raw/master/.deadlines.yml".format(
            app.config["GITLAB_URL"].rstrip("/"), app.config["COURSE_REPO"])
        raw_rsp = requests.get(deadlines_url)
        raw_rsp.raise_for_status()
        return Deadlines(yaml.safe_load(raw_rsp.content))


def _decode_dict_utf8(d):
    decoded = {}
    for k, v in d.items():
        decoded[k.decode('utf8')] = v.decode('utf8')
    return decoded

@app.route("/")
@requires_auth
def main_page():
    deadlines = Deadlines.fetch()

    task_scores = redis.StrictRedis(app.config["REDIS"]).hgetall(
        "task_score:{}:{}".format(app.config["COURSE_NAME"], session["gitlab"]["username"]))

    task_scores = _decode_dict_utf8(task_scores)

    return render_template("tasks.html",
       task_base_url=app.config["GITLAB_URL"] + app.config["COURSE_REPO"] + "/blob/master/tasks",
       deadlines=deadlines,
       task_scores=task_scores)
