import pytest
import json

from shadts.gdoc import RatingsTable, TaskNotFound
from .test_app import test_app


@pytest.fixture
def ratings_table(test_app):
    rt = RatingsTable.from_config()
    rt.clear()
    return rt

def test_sync_task_columns(ratings_table):
    ratings_table.sync_task_columns(["foo", "bar", "zog"])
    assert ["foo", "bar", "zog"] == [c.value for c in ratings_table.ws.range("H2:J2")]

    ratings_table.sync_task_columns(["zog", "zog-rejudge"])
    assert ["foo", "bar", "zog", "zog-rejudge"] == [c.value for c in ratings_table.ws.range("H2:K2")]

def test_put_score(ratings_table):
    ratings_table.sync_task_columns(["foo", "bar", "zog"])

    s1 = {
        "username": "prime",
        "repo": "https://best-cpp-course-ever.ru/1",
        "name": "Fedor Korotkiy",
        "city": "Moscow",
    }
    s2 = {
        "username": "prime2",
        "repo": "https://best-cpp-course-ever.ru/2",
        "name": "Fedor Not Korotkiy",
        "city": "New York",
    }

    with pytest.raises(TaskNotFound):
        ratings_table.put_score(s1, "foo1", lambda flags, old_score: "")

    ratings_table.put_score(s1, "foo", lambda flags, old_score: 1)
    ratings_table.put_score(s2, "zog", lambda flags, old_score: 2)
    ratings_table.put_score(s1, "zog", lambda flags, old_score: 3)

    flags = ratings_table.ws.range("D5")[0]
    flags.value = "z"
    ratings_table.ws.update_cells([flags])
    def check_flags(flags, old_score):
        assert "z" in flags
        return 1

    ratings_table.put_score(s2, "zog", check_flags)
